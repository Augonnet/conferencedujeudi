/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static Database.DatabaseUtilities.getConnexion;
import com.mysql.jdbc.Connection;
import static java.lang.String.format;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author BOULE
 */
public class Conference {

    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;
    private String nomConferencier;
    private Date sqlDate;
    
    public Conference() {

    }
    
    
    
    
    

    public Conference(int _idConference, String _titreConference, int _idConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
    }

    
     public Conference(String _titreConference, int _idConferencier, Calendar _dateConference, int _idSalle, int _idTheme) {

        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
        this.dateConference = _dateConference;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
    }   
     
    public Conference(int _idConference, String _titreConference, int _idConferencier, Calendar _dateConference, int _idSalle, int _idTheme, String _nomConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
        this.dateConference = _dateConference;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
        this.nomConferencier= _nomConferencier;
    }   

    

    
    //nomConferencier
    public String getNomConferencier(){
        return this.nomConferencier;
    }

    public void setNomConferencier(String nomConferencier){
        this.nomConferencier= nomConferencier;
    }
    
    //idConference
    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int idConference) {
        this.idConference = idConference;
    }

    //titreConference
    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String titreConference) {
        this.titreConference = titreConference;
    }

    //dateConference
    public Calendar getDateConference() {
        return this.dateConference;
    }

    public void setDateConference(Calendar dateConference) {
        this.dateConference = dateConference;
    }

    public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateConference().getTimeInMillis());
    }
    
    public java.sql.Date getSqlDate(){
        java.sql.Date sqlDate = new java.sql.Date(dateConference.getTimeInMillis());
        return sqlDate;
    }

    //idConferencier
    public int getIdConfrencier() {
        return this.idConferencier;
    }

    public void setIdConfrencier(int idConfrencier) {
        this.idConferencier = idConfrencier;
    }

    //idSalle
    public int getIdSalle() {
        return this.idSalle;
    }

    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    //idTheme
    public int getIdTheme() {
        return this.idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    
   
    

    public void insert(){
        PreparedStatement ps = null;
        Connection maConnection = getConnexion();
        String query = "INSERT INTO conference"
                + "(titreConference,dateConference, idConferencier,idSalle, idTheme) "
                + "VALUES (?,?,?,?,?)";
        
        try {
            ps = maConnection.prepareStatement(query);
            ps.setString(1, this.getTitreConference());
            ps.setDate(2, this.getSqlDate());
            ps.setInt(3, this.getConferencier().getIdConferencier());
            ps.setInt(4, getTheme().getIdTheme());
            ps.setInt(5, getSalle().getIdSalle());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        Calendar ca = Calendar.getInstance();
    }   
}
