/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import Models.ConferenceModel;
import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 * @author BOULE
 */
public class Home extends JPanel {

    int nbClic = 0;
    Calendar date = Calendar.getInstance();

    public Home() {
        JLabel lab = new JLabel("Bienvenue sur l'application");
        this.add(lab);
       
        
        
        JButton bouton = new JButton("CLIC & ...... Surprise");
        this.add(bouton);
        
        JLabel labBouton = new JLabel("");
        this.add(labBouton);
        
        
        
       
        bouton.setBounds(250, 1000, 300, 150);
        bouton.addActionListener((ActionListener) new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nbClic = nbClic + 1;
                labBouton.setText("Vous avez cliqué " + nbClic + " fois sur le bouton");
                        
               
            }
        });
    
    ArrayList<Conference> lesConf = new ArrayList();
    
    Conference conf1= new Conference(1,"Première conférence",1,date,1,1,"Mr. JACOB");
    Conference conf2= new Conference(2,"Deuxiéme conférence",2,date,2,2,"Mr. MAMENE");
    Conference conf3= new Conference(3,"Troisième conférence",3,date,3,3,"Mme SRAB");
    Conference conf4= new Conference(4,"Quatrième conférence",4,date,4,4,"Mr PITCH");
    Conference conf5= new Conference(5,"Cinquième conférence",5,date,5,5,"MR JACKYJACKY");
    
    
    lesConf.add(conf1);
    lesConf.add(conf2);
    lesConf.add(conf3);
    lesConf.add(conf4);
    lesConf.add(conf5);
    
    
    ConferenceModel confMod = new ConferenceModel(lesConf);
    
    JTable table1 = new JTable(confMod);
    table1.setSize(300,400);
    
    JScrollPane scroll = new JScrollPane (table1);
    this.add(scroll);
    }    
}
