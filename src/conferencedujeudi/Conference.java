/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import static java.lang.String.format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author BOULE
 */
public class Conference {

    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConfrencier;
    private int idSalle;
    private int idTheme;
    private String nomConferencier;
    
    public Conference() {

    }
    
    
    

    public Conference(int _idConference, String _titreConference, int _idConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConfrencier = _idConferencier;
    }

    public Conference(int _idConference, String _titreConference, int _idConferencier, Calendar _dateConference, int _idSalle, int _idTheme, String _nomConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConfrencier = _idConferencier;
        this.dateConference = _dateConference;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
        this.nomConferencier= _nomConferencier;
    }   

    

    
    //nomConferencier
    public String getNomConferencier(){
        return this.nomConferencier;
    }

    public void setNomConferencier(String nomConferencier){
        this.nomConferencier= nomConferencier;
    }
    
    //idConference
    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int idConference) {
        this.idConference = idConference;
    }

    //titreConference
    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String titreConference) {
        this.titreConference = titreConference;
    }

    //dateConference
    public Calendar getDateConference() {
        return this.dateConference;
    }

    public void setDateConference(Calendar dateConference) {
        this.dateConference = dateConference;
    }

    public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateConference().getTimeInMillis());
    }

    //idConferencier
    public int getIdConfrencier() {
        return this.idConfrencier;
    }

    public void setIdConfrencier(int idConfrencier) {
        this.idConfrencier = idConfrencier;
    }

    //idSalle
    public int getIdSalle() {
        return this.idSalle;
    }

    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    //idTheme
    public int getIdTheme() {
        return this.idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    

}
