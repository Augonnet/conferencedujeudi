/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import conferencedujeudi.Conference;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BOULE
 */
public class ConferenceModel extends AbstractTableModel{
    
    //Attribut
    private ArrayList<Conference> listeConference = new ArrayList();
    private String[] ColumnsName = new String[]{"Titre","Date","Conférencier"};
    
    
    
    
    public ConferenceModel(ArrayList<Conference> _conference){
        this.listeConference = _conference;
    }    
    
    
    public String getColumnName(int ColumnIndex){
        return ColumnsName[ColumnIndex];
    }
    
    
    
    @Override
    public int getRowCount() {
        return listeConference.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference maConf = listeConference.get(rowIndex);
        
       
        
        switch(columnIndex){
            case 0 : {
                return maConf.getTitreConference();
            }
            
            case 1 : {
                return maConf.getDateString();
            }
            case 2 : {
                return maConf.getNomConferencier();
            }
            default:
                return "";
        }
    }
     
    
}
